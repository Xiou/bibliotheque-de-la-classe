# Bibliothèque de la classe

## Pour commencer 

Il est nécessaire de posséder un localhost pour les étapes suivantes.
 
- Cloner le projet sur votre ordinateur 

Dépuis votre terminal, déterminez un emplacement pour le projet (il doit être accessible depuis votre localhost), puis clonez avec SSH le lien du git de la bibliothèque de la classe comme ceci : <code>git clone https://gitlab.com/nako_braun/bibliotheque-de-la-classe.git</code>

- Récupérer les mises à jour

Placez vous dans le dossier du projet et tapez cette commande : <code>git pull</code>

- Ajouter une nouvelle version du fichier

<code>git add <lefichier.format></code>

<code>git commit -m "entrez un message qui explique le changement effectué"></code>

<code>git push</code>

## Créer sa propre interface Zotero avec PHP

https://gitlab.com/EtienneOz/wwwahou/-/tree/master/vendor/hedii/zotero-api#installation

- Téléchargez Composer globalement depuis votre terminal : https://getcomposer.org/download/

Composer est un outil de gestion des dépendances en PHP. Il vous permet de déclarer les bibliothèques dont dépend votre projet et il les gérera (installation/mise à jour) pour vous.
 
- Dans votre dossier, créez un fichier : composer.json

https://getcomposer.org/doc/01-basic-usage.md

Ce fichier décrit les dépendances de votre projet et peut également contenir d'autres métadonnées. Il doit généralement être placé dans le répertoire le plus élevé de votre projet/référentiel.

- Copiez le requiere suivant dans votre fichier composer.json : <code>{"require":{"hedii/zotero-api": "^1.1","adibaba/zotero-api-client": "^0.0.2"}}</code>

Vous indiquez à Composer de quels paquets dépendent votre projet.

Ce paquet a été trouvé sur https://packagist.org/

- Puis dans votre terminal tapez : <code>composer require hedii/zotero-api</code> et <code>composer require adibaba/zotero-api-client</code>

- Installer ensuite les dépendances : https://getcomposer.org/doc/01-basic-usage.md#installing-dependencies
