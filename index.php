<?php
 include('inc/header.php');

// on recupère l'API de notre Zotero
 $url = file_get_contents('https://api.zotero.org/groups/4412339/items?limit=1000');
  // file_get_contents() permet de retourner le fichier dans une chaîne de caractères

// on crée un tableau associatif de nos données
 $array = json_decode($url, TRUE);
  // JSON est un format de fichier standard ouvert, c'est un format d'échange de données qui utilise du texte lisible par l'homme
    // TRUE = les objets JSON seront renvoyés sous forme de tableaux associatifs
    // FALSE = les objets JSON seront renvoyés sous forme d'objets.
    // NULL = les objets JSON sont renvoyés sous forme de tableaux associatifs ou d'objets

 $items = array_merge($array);
 // echo $items;

// mon code PHP

// echo '<main>';
//   echo '<div class="items">';
//     foreach ($items as $item) {
//       $data = $item['data'];
//       $title = $data['title'];
//       $url = $data['url'];
//       $date = substr($data['dateAdded'], 0, 10);
//       $abstract = $data['abstractNote'];
//       $tags = $data['tags'];
//   echo '</div>';
// echo '</main>';


// le code PHP d'Étienne Ozeray

   // <!-- $allTags = [];
   // $authors = [];
   // foreach ($items as $item) {
   //   $tags = $item['data']['tags'];
   //   foreach ($tags as $tag) {
   //     if ($tag['tag'] != 'notShowed' || $tag['tag'] != 'cambreOk' || $tag['tag'] != 'pourMoi') {
   //       array_push($allTags, $tag['tag']);
   //     }
   //   }
   // }
   //
   // $allTags = array_unique($allTags);
   //
   //  echo '<ul class="tags">';
   //    echo '<li><h1>Wwwahou </h1></li>';
   //    foreach ($allTags as $tag) {
   //      if ($tag != 'notShowed' && $tag != 'cambreOk' && $tag != 'pourMoi' && substr($tag, 0, 4) != 'auth') {
   //      echo '~ <li class="tag '. $tag .'">' . $tag . '</li> ';
   //      }
   //    }
   //  echo '</ul>';

    // echo '<div class="items">';

     foreach ($items as $item) {
       $data = $item['data'];
       // if (is_array($data) && in_array('book', $data)) {
       // if (is_array($data)) {
         $title = $data['title'];
          // echo $title;
         $url = $data['url'];
         $date = substr($data['dateAdded'], 0, 10);
         $note = $data['abstractNote'];
         $tags = $data['tags'];
         $language = $data['language'];
         $format = $data['itemType'];
         $user = $data['creator'];
         // if ($url) {
           # code...
           echo '<div class="item">';
             echo '<section class="info">';

               echo '<span class="date">' . $date . ' </span>';
               echo '<span class="title">' . $title . ' </span>';
               echo '<a href="' . $url . '"target="_blank">' . ($title ? $title : '') . '</a>';
               echo '<span class="note">' . $note . '</span>';
               echo '<span class="language">' . $language . '</span>';
               echo '<span class="format">' . $format . '</span>';
               echo '<span class="user">' . $user . '</span>';
               // echo '<sup>';
               //   foreach ($tags as $tag) {
               //     if ($tag['tag'] != 'notShowed' && $tag['tag'] != 'cambreOk' && $tag['tag'] != 'pourMoi' && substr($tag['tag'], 0, 4) != 'auth') {
               //       echo ' <span>' . $tag['tag'] . '</span>';
               //     }
               //   }
               // echo '</sup>';
               foreach ($tags as $tag) {
                 if (substr($tag['tag'], 0, 4) == 'auth') {
                   echo '<span class="author"> proposé par ' . substr($tag['tag'], 7) . '</span>';
                 }
               }

             echo '</section>';
             // echo ' <span>'. $tags . '</span>';
             // echo '<div class="iframe"><iframe data-src="'. $url .'"></iframe></div>';
           echo '</div>';
         // }
       // }
     }
   // echo '</section>';


   include('inc/footer.php');
?>
